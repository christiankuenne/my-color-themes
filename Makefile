install:
	# Copy all required files to /opt
	mkdir -p /opt/my-color-themes
	cp -r schemes /opt/my-color-themes/
	cp -r templates /opt/my-color-themes/
	cp my-color-themes.sh /opt/my-color-themes/
	# Wrapper script in bin
	cp setup/my-color-themes /usr/local/bin/

uninstall:
	rm -rf /opt/my-color-themes
	rm /usr/local/bin/my-color-themes

clean:
	rm -rf ~/.my-color-themes
