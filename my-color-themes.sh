#!/bin/bash

TEMP_FOLDER="$HOME/.my-color-themes";

show_help() {
    echo 'Usage: my-color-themes [THEME]';
    echo 'base16 color theme selector';
    echo '';
    echo 'Argument:';
    echo '  THEME: The name of the color theme. A selection is shown if no argument is provided.';
}

# Apply Color Scheme
# $1 Color Scheme File
apply_scheme() {
    # Dunst
    mkdir -p ~/.config/dunst
    apply "$1" templates/dunst/default.mustache > ~/.config/dunst/dunstrc
    killall dunst
    # GTK Source View
    mkdir -p ~/.local/share/gtksourceview-3.0/styles
    apply "$1" templates/gtksourceview/default.mustache > ~/.local/share/gtksourceview-3.0/styles/base16-theme.xml
    # Rofi
    mkdir -p ~/.local/share/rofi/themes
    apply "$1" templates/rofi/default.mustache > ~/.local/share/rofi/themes/base16-theme.rasi
    # VS Code
    mkdir -p ~/.vscode/extensions/base16-theme/themes
    cp templates/vscode/package.json ~/.vscode/extensions/base16-theme/
    apply "$1" templates/vscode/default.mustache > ~/.vscode/extensions/base16-theme/themes/theme.json
    # xdm
    #apply "$1" templates/xdm/Xresources.mustache > output/Xresources
    #apply "$1" templates/xdm/Xsetup.mustache > output/Xsetup
    # Xresources
    mkdir -p ~/.Xresources.d
    apply "$1" templates/xresources/default.mustache > ~/.Xresources.d/base16-theme
    xrdb merge ~/.Xresources
}

# Apply Color Theme to Template
# $1 Color Scheme File
# $2 Template File
apply() {
    local template=`cat "$2"`;
    while read line; do
        local key=`echo "$line" | sed -n 's/\(.*\): ".*/\1/p'`;
        if [ -n "$key" ];
        then
            local value=`echo "$line" | sed -n 's/[^:]*: "\(.*\)".*/\1/p'`;
            local placeholder=`get_placeholder "$key"`;
            template=`replace "$template" "$placeholder" "$value"`;
        fi
    done < "$1"
    echo "$template";
}

# Replace Placeholder with Value
# $1 Template File
# $2 Placeholder
# $3 Value
replace() {
    echo "$1" | sed "s~$2~$3~gi";
}

# Get Placeholder
# $1 Key
get_placeholder() {
    if [[ "$1" == base* ]];
    then
        echo "{{$1-hex}}";
    elif [[ "$1" == 'scheme' ]];
    then
        echo "{{scheme-name}}";
    elif [[ "$1" == 'author' ]];
    then
        echo "{{scheme-author}}";
    else
        echo "{{$1}}"
    fi
}

# Show Menu
menu() {
    # Scheme Selection
    theme=$(ls schemes/ | cut -d "." -f 1 | rofi -dmenu -i -p "Select Color Theme");
    if [ "$theme" ]; then
        apply_scheme "schemes/$theme.yaml"
    fi
}

# Main
if [ $# == 0 ]; then
    menu;
elif [ $# == 1 ]; then
    if [[ "$1" == '-h' ]] || [[ "$1" == '--help' ]]; then
        show_help;
    else
        apply_scheme "$1"
    fi
else
    show_help;
    exit 1;
fi