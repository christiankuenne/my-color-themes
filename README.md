# My Color Themes

Color theme switcher with my favourite [base16](https://github.com/chriskempson/base16) color themes. The selected theme is applied to the following applications:

-   Dunst
-   Mousepad (GtkSourceView)
-   Rofi
-   Simple Terminal (.Xresources)
-   VS Code

**Caution!** The theme switcher is going to overwrite your dotfiles!

## Prerequisites

[Rofi](https://github.com/davatorium/rofi) is used for the selection.

.Xresources must contain the following line:

```
#include ".Xresources.d/base16-theme"
```

## Getting Started

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

Clean temp files:

```
make clean
```

## Usage

Show color themes and apply the selected theme:

```
my-color-themes
```

Apply a specfic theme:

```
my-color-themes schemes/solarflare.yaml
```

## Won't do

I tried [Themix/Oomox][https://github.com/themix-project/oomox], but never managed to generate a nice looking GTK theme.
Decided to not use base16 for GTK themes.

It is possible to apply the new theme to a running instance of ST, but it's ugly and I don't need it. Removed.

## Might do

-   xdm (needs sudo)
-   Preview? (just generate HTML)
-   Accent color?
    -   Selections
    -   Borders
